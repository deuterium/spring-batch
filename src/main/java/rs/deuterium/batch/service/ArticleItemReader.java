package rs.deuterium.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import rs.deuterium.batch.entity.Content;
import rs.deuterium.batch.entity.Element;

import java.util.List;

/**
 * Created by MilanNuke 06-May-19
 */

@Component
public class ArticleItemReader implements ItemReader<Content> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String ARTICLE_API_URL = "http://localhost:8085/articles?size=10&page=";

    private int nextIndex;
    private List<Content> contents;

    private final RestTemplate restTemplate;

    public ArticleItemReader(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.nextIndex = 0;
    }


    @Override
    public Content read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        logger.info("Reader - nextIndex: {}", nextIndex);
        if(contentsIsNotInitialized()){
            contents = fetchArticlesFromApi(nextIndex);
        }

        Content nextContent = null;

        if (nextIndex < contents.size()) {
            nextContent = contents.get(nextIndex);
            nextIndex++;
        }

        return nextContent;
    }

    private List<Content> fetchArticlesFromApi(int nextIndex) {
        logger.info("Fetching Articles for index = {}", nextIndex);
        ResponseEntity<Element> response = restTemplate.getForEntity(ARTICLE_API_URL + nextIndex, Element.class);
        Element element = response.getBody();
        return element.getContent();

    }

    private boolean contentsIsNotInitialized() {
        return this.contents == null;
    }
}
