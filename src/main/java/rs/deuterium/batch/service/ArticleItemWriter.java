package rs.deuterium.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import rs.deuterium.batch.model.Article;

import java.util.List;

/**
 * Created by MilanNuke 06-May-19
 */

@Component
public class ArticleItemWriter implements ItemWriter<Article> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void write(List<? extends Article> list) throws Exception {
        list.forEach(a -> logger.info("Writer: {}, {}", a.getId(), a.getName()));
        logger.info("End Writer");
    }
}
