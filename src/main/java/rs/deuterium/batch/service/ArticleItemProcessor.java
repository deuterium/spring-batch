package rs.deuterium.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import rs.deuterium.batch.entity.Content;
import rs.deuterium.batch.model.Article;

/**
 * Created by MilanNuke 06-May-19
 */

@Component
public class ArticleItemProcessor implements ItemProcessor<Content, Article> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Article process(Content content) throws Exception {
        logger.info("Processor: {}", content.getTitle());
        return new Article(content.getUuid(), content.getTitle());
    }
}
