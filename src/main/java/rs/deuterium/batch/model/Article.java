package rs.deuterium.batch.model;

/**
 * Created by MilanNuke 06-May-19
 */
public class Article {

    private String id;
    private String name;

    public Article() {
    }

    public Article(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
