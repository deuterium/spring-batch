package rs.deuterium.batch.entity;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MilanNuke 06-May-19
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "uuid",
        "title",
        "receivedDate",
        "abstractText",
        "type",
        "status",
        "correspondingAuthorEmail",
        "manuscriptUploadUuid",
        "graphAbstractUploadUuid",
        "supplementaryUploadUuid",
        "suggestedReviewers",
        "authors",
        "keywords",
        "aaDetails"
})
public class Content {

    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("title")
    private String title;
    @JsonProperty("receivedDate")
    private String receivedDate;
    @JsonProperty("abstractText")
    private AbstractText abstractText;
    @JsonProperty("type")
    private String type;
    @JsonProperty("status")
    private String status;
    @JsonProperty("correspondingAuthorEmail")
    private String correspondingAuthorEmail;
    @JsonProperty("manuscriptUploadUuid")
    private String manuscriptUploadUuid;
    @JsonProperty("graphAbstractUploadUuid")
    private String graphAbstractUploadUuid;
    @JsonProperty("supplementaryUploadUuid")
    private Object supplementaryUploadUuid;
    @JsonProperty("suggestedReviewers")
    private String suggestedReviewers;
    @JsonProperty("authors")
    private List<Author> authors = null;
    @JsonProperty("keywords")
    private List<Keyword> keywords = null;
    @JsonProperty("aaDetails")
    private Object aaDetails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("receivedDate")
    public String getReceivedDate() {
        return receivedDate;
    }

    @JsonProperty("receivedDate")
    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    @JsonProperty("abstractText")
    public AbstractText getAbstractText() {
        return abstractText;
    }

    @JsonProperty("abstractText")
    public void setAbstractText(AbstractText abstractText) {
        this.abstractText = abstractText;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("correspondingAuthorEmail")
    public String getCorrespondingAuthorEmail() {
        return correspondingAuthorEmail;
    }

    @JsonProperty("correspondingAuthorEmail")
    public void setCorrespondingAuthorEmail(String correspondingAuthorEmail) {
        this.correspondingAuthorEmail = correspondingAuthorEmail;
    }

    @JsonProperty("manuscriptUploadUuid")
    public String getManuscriptUploadUuid() {
        return manuscriptUploadUuid;
    }

    @JsonProperty("manuscriptUploadUuid")
    public void setManuscriptUploadUuid(String manuscriptUploadUuid) {
        this.manuscriptUploadUuid = manuscriptUploadUuid;
    }

    @JsonProperty("graphAbstractUploadUuid")
    public String getGraphAbstractUploadUuid() {
        return graphAbstractUploadUuid;
    }

    @JsonProperty("graphAbstractUploadUuid")
    public void setGraphAbstractUploadUuid(String graphAbstractUploadUuid) {
        this.graphAbstractUploadUuid = graphAbstractUploadUuid;
    }

    @JsonProperty("supplementaryUploadUuid")
    public Object getSupplementaryUploadUuid() {
        return supplementaryUploadUuid;
    }

    @JsonProperty("supplementaryUploadUuid")
    public void setSupplementaryUploadUuid(Object supplementaryUploadUuid) {
        this.supplementaryUploadUuid = supplementaryUploadUuid;
    }

    @JsonProperty("suggestedReviewers")
    public String getSuggestedReviewers() {
        return suggestedReviewers;
    }

    @JsonProperty("suggestedReviewers")
    public void setSuggestedReviewers(String suggestedReviewers) {
        this.suggestedReviewers = suggestedReviewers;
    }

    @JsonProperty("authors")
    public List<Author> getAuthors() {
        return authors;
    }

    @JsonProperty("authors")
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @JsonProperty("keywords")
    public List<Keyword> getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("aaDetails")
    public Object getAaDetails() {
        return aaDetails;
    }

    @JsonProperty("aaDetails")
    public void setAaDetails(Object aaDetails) {
        this.aaDetails = aaDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
