package rs.deuterium.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import rs.deuterium.batch.entity.Content;
import rs.deuterium.batch.model.Article;


/**
 * Created by MilanNuke 06-May-19
 */

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    @Autowired
    private ItemReader<Content> itemReader;
    @Autowired
    private ItemProcessor<Content, Article> itemProcessor;
    @Autowired
    private ItemWriter<Article> itemWriter;


    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<Content, Article>chunk(10)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();
    }

    @Bean
    public Job exportArticleJob() {
        return jobBuilderFactory
                .get("consumeWebService")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }


}
